--ELIMINAR UNA BASE DE DATOS
if exists (select 1 from sys.databases where name='db_academico') 
begin
	drop database db_academico;
end;

--CREAR UNA BASE DE DATOS
if not exists (select 1 from sys.databases where name='db_academico') 
begin
	create database db_academico;
end;


--CREANDO TABLAS
/*
EMPLEADOS
TRABAJOS
UNIVERSIDADES
AREAS_ACADEMICAS
ESTUDIOS
HISTORIAL_LABORAL
*/

use db_academico;

CREATE TABLE EMPLEADOS (
	id_emp INT NOT NULL IDENTITY(1,1),
	dni_emp CHAR(8) NOT NULL,
	nombres_emp VARCHAR(100),
	apepat_emp VARCHAR (50),
	apemat_emp VARCHAR (50),
	direccion1_emp VARCHAR (200),
	direccion2_emp VARCHAR (200),
	departamento_emp INT,
	provincia_emp INT,
	distrito_emp INT,
	cod_postal_emp VARCHAR(5),
	sexo_emp CHAR(1),
	fecnac_emp DATE
)


CREATE TABLE TRABAJOS (
	id_trab INT NOT NULL IDENTITY(1,1),
	nombre_trab VARCHAR(100),
	salario_min_trab NUMERIC(7,2),
	salario_max_trab NUMERIC(7,2)
)

CREATE TABLE UNIVERSIDADES (
	id_univ INT NOT NULL IDENTITY(1,1),
	codigo_univ CHAR(10),
	nombre_univ VARCHAR(200),
	departamento_univ INT,
	provincia_univ INT,
	distrito_univ INT,
	direccion_univ VARCHAR(200),
	cod_postal_univ VARCHAR(5)
)

CREATE TABLE AREAS_ACADEMICAS (
	id_area INT NOT NULL IDENTITY(1,1),
	codigo_area CHAR(5),
	nombre_area VARCHAR(100),
	unidad_superior_area INT,
	presupuesto_area NUMERIC(7,2),
	presupuesto_actual_area NUMERIC(7,2)
)

DROP TABLE ESTUDIOS;

CREATE TABLE ESTUDIOS (
	id_emp INT NOT NULL,
	id_univ INT NOT NULL,
	anio_ingreso_emp CHAR(4),
	anio_egreso_emp CHAR(4),
	grado_emp VARCHAR(100),
	especialidad_emp VARCHAR(100)
)

CREATE TABLE HISTORIAL_LABORAL (
	id_emp INT NOT NULL,
	id_trab INT NOT NULL,
	fecha_inicio DATE,
	fecha_fin DATE,
	id_area  INT NOT NULL,
	supervisor  INT
)

--CREANDO CLAVES PRIMARIAS
ALTER TABLE EMPLEADOS ADD CONSTRAINT PK_EMPLEADOS PRIMARY KEY (id_emp);
ALTER TABLE TRABAJOS ADD CONSTRAINT PK_TRABAJOS PRIMARY KEY (id_trab);
ALTER TABLE UNIVERSIDADES ADD CONSTRAINT PK_UNIVERSIDADES PRIMARY KEY (id_univ);
ALTER TABLE AREAS_ACADEMICAS ADD CONSTRAINT PK_AREAS_ACADEMICAS PRIMARY KEY (id_area);
ALTER TABLE ESTUDIOS ADD CONSTRAINT PK_ESTUDIOS PRIMARY KEY (id_emp, id_univ);
ALTER TABLE HISTORIAL_LABORAL ADD CONSTRAINT PK_HISTORIAL_LABORAL PRIMARY KEY (id_emp, id_trab, id_area);

--CREADNDO CLAVES EXTERNAS
ALTER TABLE ESTUDIOS ADD CONSTRAINT FK_ESTUDIOS_EMP FOREIGN KEY (id_emp)
REFERENCES EMPLEADOS (id_emp) ON DELETE CASCADE;

ALTER TABLE ESTUDIOS ADD CONSTRAINT FK_ESTUDIOS_UNIV FOREIGN KEY (id_univ)
REFERENCES UNIVERSIDADES (id_univ) ON DELETE CASCADE;
 
ALTER TABLE HISTORIAL_LABORAL ADD CONSTRAINT FK_HISTORIAL_LABORAL_EMP FOREIGN KEY (id_emp)
REFERENCES EMPLEADOS (id_emp) ON DELETE CASCADE;

ALTER TABLE HISTORIAL_LABORAL ADD CONSTRAINT FK_HISTORIAL_LABORAL_TRAB FOREIGN KEY (id_trab)
REFERENCES TRABAJOS (id_trab) ON DELETE CASCADE;

ALTER TABLE HISTORIAL_LABORAL ADD CONSTRAINT FK_HISTORIAL_LABORAL_AREA FOREIGN KEY (id_area)
REFERENCES AREAS_ACADEMICAS (id_area) ON DELETE CASCADE;

--Insertando datos a empleados
INSERT INTO EMPLEADOS VALUES ('06181541','LUCY INES','FLORES','GUTARRA','URB PALOMINO BLK B-2-31','','','','','','F','2005-05-13');
INSERT INTO EMPLEADOS VALUES ('10005896','MANUEL AUGUSTO','MARIATEGUI','CRESPO','ALAMEDA DEL ARCO IRIS NRO.LT13  MZA.A','','','','','','M','1995-04-24');
INSERT INTO EMPLEADOS VALUES ('07240173','RAFAEL LEONCIO','CONTRERAS','ROSALES','CALLE RUBENS 260','','','','','','M','1979-06-14');
INSERT INTO EMPLEADOS VALUES ('08547600','JOSE BENIGNO','MARIN','ABANTO','CALLE PROLONG.PIMENTEL MZ.B LT.23 URB.VILLA DEL NORTE','','','','','','M','1990-01-12');
INSERT INTO EMPLEADOS VALUES ('06856351','LUIS ALBERTO','OLORTEGUI','GALVEZ','JR. CUBA sn DPTO 201 MZ K1 LT 26 ETP 1 URB. SANTA PATRICIA','','','','','','M','2000-03-16');
INSERT INTO EMPLEADOS VALUES ('00488458','GEINER RAUL','MELENDEZ','LIENDO','P. JOVEN PARA CHICO CALLE SAMUEL ALCAZAR 963','','','','','','M','2001-12-02');
INSERT INTO EMPLEADOS VALUES ('25438042','JOSE GABRIEL','GONZALES','VELARDE','BARRIO FISCAL 1 73','','','','','','M','1999-02-03');
INSERT INTO EMPLEADOS VALUES ('06926698','CARLOS ERNESTO','FLORIAN','MONSALVE','LA UNION 620 PABLO VI','','','','','','M','1998-04-04');
INSERT INTO EMPLEADOS VALUES ('08783314','ZOILA MARIA','MELENDEZ','PE�A','CAL. DAVID ROCA VAREA 234 VISTA ALEGRE','','','','','','F','1999-11-09');
INSERT INTO EMPLEADOS VALUES ('07946110','ROSA PAQUITA','SARRIA','GARCIA DE BRUMMER','CALLE PASEO REAL  MZA.D-LOTE 9','','','','','','F','2002-09-07');
INSERT INTO EMPLEADOS VALUES ('08539174','CELINDA LUZ','SARZO','ROJAS','JR. PUNO N? 3681 URB. PERU','','','','','','F','2003-10-14');
INSERT INTO EMPLEADOS VALUES ('08267641','FRANCA MARISA','FARGION','BARRON','CALLE LOS OLIVOS 204 DPTO 41','','','','','','F','2001-05-03');
INSERT INTO EMPLEADOS VALUES ('08444463','ANA BEATRIZ','BORJA','SANTA CRUZ','AV. ESCARDO 660 DPTO 505 URB. MARANGA','','','','','','F','2004-10-21');
INSERT INTO EMPLEADOS VALUES ('25661252','GINO GREGORIO','CAMPA�A','ALBAN','JR. JOSUE SAAVEDRA 187','','','','','','M','1999-03-23');
INSERT INTO EMPLEADOS VALUES ('09536809','ERMO JOSE','MAC DOWALL','MU�OZ','CALLE CESAR VALLEJO NRO.196','','','','','','M','2001-08-19');
INSERT INTO EMPLEADOS VALUES ('05640370','CARLOS RICARDO','CHACON','BEJAR','CAL. LOS ALAMOS 292 DPTO LIMA INT 703 URB. ORRANTIA','','','','','','M','2003-04-16');
INSERT INTO EMPLEADOS VALUES ('08040947','DELIA EMPERATRIZ','FARIAS','CESPEDES','MZ E LT 1 ENTRE CALLE 2 Y 5 URB. LA LIBERTAD','','','','','','F','2006-05-18');
INSERT INTO EMPLEADOS VALUES ('10373843','BEATRIZ URSULA','IBARGUEN','AVELLANEDA','AV. CENTRAL 960 DPTO 303 BLOCK B11 URB. LOS ALAMOS MONTERRIC','','','','','','F','1997-07-25');
INSERT INTO EMPLEADOS VALUES ('03508193','JORGE LUIS','FARIAS','MORAN','CAL. SAN ROMAN 205 OTR. CERCADO TUMBES','','','','','','M','2003-04-15');
INSERT INTO EMPLEADOS VALUES ('06064941','RAUL','RIOS','SACA','CA COLAN 117-123 URB. LOS TULIPANES','','','','','','M','1997-01-12');

--insertando trabajos
INSERT INTO TRABAJOS VALUES ('ASISTENTE DE ORIENTACION INFORMATICO','1517.53','3017.53');
INSERT INTO TRABAJOS VALUES ('INTENDENTE DE ADUANA           ','1216.38','2716.38');
INSERT INTO TRABAJOS VALUES ('ASISTENTE ACADEMICO ESPECIALIZADO','590.83','2090.83');
INSERT INTO TRABAJOS VALUES ('JEFE ENCARGADO','2769.9','4269.9');
INSERT INTO TRABAJOS VALUES ('ASISTENTE DE ORIENTACION ACADEMICO','1654.52','3154.52');
INSERT INTO TRABAJOS VALUES ('VERIFICADOR','4449.52','5949.52');
INSERT INTO TRABAJOS VALUES ('ANALISTA DE ATENCION A USUARIOS','4122.43','5622.43');
INSERT INTO TRABAJOS VALUES ('ANALISTA DE HELP DESK','2110.76','3610.76');
INSERT INTO TRABAJOS VALUES ('ANALISTA DE CONTROL DE CALIDAD','979.16','2479.16');
INSERT INTO TRABAJOS VALUES ('FEDATARIO FISCALIZADOR','2596.09','4096.09');
INSERT INTO TRABAJOS VALUES ('ANALISTA DE SISTEMAS','3285.73','4785.73');
INSERT INTO TRABAJOS VALUES ('APOYO PROFESIONAL DE PRESUPUESTO','3019.51','4519.51');
INSERT INTO TRABAJOS VALUES ('ANALISTA DE PRECIOS','2398.51','3898.51');
INSERT INTO TRABAJOS VALUES ('TECNICO II','3820.69','5320.69');
INSERT INTO TRABAJOS VALUES ('INSTRUCTOR TIC','2293.67','3793.67');
INSERT INTO TRABAJOS VALUES ('REVISOR DE EQUIPAJE','3567.32','5067.32');
INSERT INTO TRABAJOS VALUES ('COORDINADOR DE ADUANAS DESCONCENTRADAS  ','550.98','2050.98');
INSERT INTO TRABAJOS VALUES ('PROGRAMADOR JUNIOR','1720.54','3220.54');
INSERT INTO TRABAJOS VALUES ('APOYO DE OFICIALES DE ADUANA','1598.99','3098.99');


--insertando universidades
INSERT INTO UNIVERSIDADES VALUES ('UNMSM','UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS','','','','Jr. Conde de Superunda 141','');
INSERT INTO UNIVERSIDADES VALUES ('UNSCH','UNIVERSIDAD NACIONAL DE SAN CRIST�BAL DE HUAMANGA','','','','Av. Pedro de Osma 423','');
INSERT INTO UNIVERSIDADES VALUES ('UNSAAC','UNIVERSIDAD NACIONAL DE SAN ANTONIO ABAD DEL CUSCO','','','','Av. Grau 1511','');
INSERT INTO UNIVERSIDADES VALUES ('UNT','UNIVERSIDAD NACIONAL DE TRUJILLO','','','','Av. Pedro de Osma 105','');
INSERT INTO UNIVERSIDADES VALUES ('UNSAA','UNIVERSIDAD NACIONAL DE SAN AGUST�N DE AREQUIPA','','','','Av. La Universidad s/n','');
INSERT INTO UNIVERSIDADES VALUES ('UNI','UNIVERSIDAD NACIONAL DE INGENIER�A','','','','Av. T�pac Amaru 210 (Km. 4.5 )','');
INSERT INTO UNIVERSIDADES VALUES ('UNA','UNIVERSIDAD NACIONAL AGRARIA LA MOLINA','','','','Av. Universitaria 1801 ','');
INSERT INTO UNIVERSIDADES VALUES ('UNSLG','UNIVERSIDAD NACIONAL SAN LUIS GONZAGA','','','','Av. Arenales 1256','');
INSERT INTO UNIVERSIDADES VALUES ('UNCP','UNIVERSIDAD NACIONAL DEL CENTRO DEL PER�','','','','Av. Benavides 5440','');
INSERT INTO UNIVERSIDADES VALUES ('UNAP','UNIVERSIDAD NACIONAL DE LA AMAZON�A PERUANA','','','','Av. Venezuela cuadra 34','');
INSERT INTO UNIVERSIDADES VALUES ('UNDA','UNIVERSIDAD NACIONAL DEL ALTIPLANO','','','','Av. Riva-Ag�ero Cdra 8 s/n ','');
INSERT INTO UNIVERSIDADES VALUES ('UNP','UNIVERSIDAD NACIONAL DE PIURA','','','','Av. Las Leyendas 580 ','');
INSERT INTO UNIVERSIDADES VALUES ('UNC','UNIVERSIDAD NACIONAL DE CAJAMARCA','','','','Av. Paseo de la Rep�blica 250','');
INSERT INTO UNIVERSIDADES VALUES ('UNFV','UNIVERSIDAD NACIONAL FEDERICO VILLARREAL','','','','Av. Paseo Col�n 125','');
INSERT INTO UNIVERSIDADES VALUES ('UNAS','UNIVERSIDAD NACIONAL AGRARIA DE LA SELVA','','','','Av. Paseo de la Rep�blica 250','');
INSERT INTO UNIVERSIDADES VALUES ('UNHV','UNIVERSIDAD NACIONAL HERMILIO VALDIZ�N DE HU�NUCO','','','','Av. Nicol�s de Pi�rola 1222','');
INSERT INTO UNIVERSIDADES VALUES ('UNEGV','UNIVERSIDAD NACIONAL DE EDUCACI�N ENRIQUE GUZM�N Y VALLE','','','','Esquina Jr. Lampa y Jr. Ucayali','');

--Insertando Areas Academicas
INSERT INTO AREAS_ACADEMICAS VALUES ('GA','GERENCIA ACADEMICA','10','14130.12','11185');
INSERT INTO AREAS_ACADEMICAS VALUES ('SP','SUPERVICION DE PRACTICAS','3','16153.9','15750');
INSERT INTO AREAS_ACADEMICAS VALUES ('DI','DEPARTAMENTO DE INSTRUCTORES','9','17163.79','11311');
INSERT INTO AREAS_ACADEMICAS VALUES ('FA','FORMACION ACADEMICA','1','13175.78','17183');
INSERT INTO AREAS_ACADEMICAS VALUES ('DL','DIRECCION DE LABORATORIOS','5','16629.6','19179');
INSERT INTO AREAS_ACADEMICAS VALUES ('SA','SUPERVISION ACADEMICA','6','18822.66','19241');
INSERT INTO AREAS_ACADEMICAS VALUES ('SB','SUPERVISION DE BOLETAS','8','14517.2','13804');
INSERT INTO AREAS_ACADEMICAS VALUES ('RH','DIVISION DE RECURSOS HUMANOS','2','19822.03','11243');
INSERT INTO AREAS_ACADEMICAS VALUES ('DL','DIVISION DE LABORATORIOS','4','19972.1','16000');
INSERT INTO AREAS_ACADEMICAS VALUES ('SD','SUPERVISION DE DEPORTES','9','13620.82','10751');

--Insertando datos a Estudios
INSERT INTO ESTUDIOS VALUES ('20','10','2010','2014','BACHILLER','TECNOLOGIAS DE INFORMACION');
INSERT INTO ESTUDIOS VALUES ('11','15','2007','2011','TECNICO','REDES Y TELECOMUNICACIONES');
INSERT INTO ESTUDIOS VALUES ('11','6','2009','2013','INGENIERO','MECANICA AUTOMOTRIZ');
INSERT INTO ESTUDIOS VALUES ('12','12','2017','2021','ADMINISTRADOR','TECNOLOGIAS DE INFORMACION');
INSERT INTO ESTUDIOS VALUES ('5','7','2010','2014','INGENIERO','REDES Y TELECOMUNICACIONES');
INSERT INTO ESTUDIOS VALUES ('11','11','2006','2010','TECNICO','MECANICA AUTOMOTRIZ');
INSERT INTO ESTUDIOS VALUES ('9','11','2011','2015','TECNICO','TECNOLOGIAS DE INFORMACION');
INSERT INTO ESTUDIOS VALUES ('19','10','2005','2009','ADMINISTRADOR','REDES Y TELECOMUNICACIONES');
INSERT INTO ESTUDIOS VALUES ('9','1','2015','2019','ADMINISTRADOR','MECANICA AUTOMOTRIZ');
INSERT INTO ESTUDIOS VALUES ('19','4','2017','2021','BACHILLER','TECNOLOGIAS DE INFORMACION');
INSERT INTO ESTUDIOS VALUES ('14','9','2008','2012','BACHILLER','REDES Y TELECOMUNICACIONES');
INSERT INTO ESTUDIOS VALUES ('2','17','2005','2009','BACHILLER','MECANICA AUTOMOTRIZ');
INSERT INTO ESTUDIOS VALUES ('5','5','2011','2015','LICENCIADO','TECNOLOGIAS DE INFORMACION');
INSERT INTO ESTUDIOS VALUES ('12','2','2009','2013','LICENCIADO','REDES Y TELECOMUNICACIONES');
INSERT INTO ESTUDIOS VALUES ('7','6','2013','2017','ABOGADO','MECANICA AUTOMOTRIZ');
INSERT INTO ESTUDIOS VALUES ('19','11','2016','2020','CONTADOR','TECNOLOGIAS DE INFORMACION');
INSERT INTO ESTUDIOS VALUES ('5','1','2016','2020','CONTADOR','REDES Y TELECOMUNICACIONES');
INSERT INTO ESTUDIOS VALUES ('11','14','2010','2014','ABOGADO','MECANICA AUTOMOTRIZ');
INSERT INTO ESTUDIOS VALUES ('11','2','2015','2019','CONTADOR','TECNOLOGIAS DE INFORMACION');
INSERT INTO ESTUDIOS VALUES ('18','16','2008','2012','TECNICO','REDES Y TELECOMUNICACIONES');

--INSERTANDO DATOS A HISTORIAL_LABORAL
INSERT INTO HISTORIAL_LABORAL VALUES ('17','6','2016-07-5','2019-05-10','1','8');
INSERT INTO HISTORIAL_LABORAL VALUES ('18','16','2002-01-12','2015-03-28','7','7');
INSERT INTO HISTORIAL_LABORAL VALUES ('7','9','2006-04-23','2016-02-11','3','1');
INSERT INTO HISTORIAL_LABORAL VALUES ('12','9','2000-07-26','2005-08-7','10','11');
INSERT INTO HISTORIAL_LABORAL VALUES ('17','19','1998-05-16','2014-01-2','8','12');
INSERT INTO HISTORIAL_LABORAL VALUES ('12','1','2001-04-22','2003-10-27','4','1');
INSERT INTO HISTORIAL_LABORAL VALUES ('17','14','2010-11-7','2011-05-4','10','12');
INSERT INTO HISTORIAL_LABORAL VALUES ('8','6','2004-11-6','2010-05-26','9','8');
INSERT INTO HISTORIAL_LABORAL VALUES ('10','3','1998-05-25','2002-01-9','3','15');
INSERT INTO HISTORIAL_LABORAL VALUES ('13','11','2001-12-16','2006-05-17','9','9');
INSERT INTO HISTORIAL_LABORAL VALUES ('7','9','2000-07-9','2001-07-30','6','6');
INSERT INTO HISTORIAL_LABORAL VALUES ('6','12','1995-06-20','2008-11-26','2','5');
INSERT INTO HISTORIAL_LABORAL VALUES ('3','18','2005-03-27','2012-04-3','1','8');
INSERT INTO HISTORIAL_LABORAL VALUES ('12','5','1997-09-14','2006-08-28','9','15');
INSERT INTO HISTORIAL_LABORAL VALUES ('19','11','2001-07-19','2009-07-21','5','13');
INSERT INTO HISTORIAL_LABORAL VALUES ('7','18','2001-08-7','2004-09-10','2','19');
INSERT INTO HISTORIAL_LABORAL VALUES ('18','1','1998-09-8','2003-08-14','3','17');
INSERT INTO HISTORIAL_LABORAL VALUES ('9','15','1999-04-17','2007-04-27','5','9');
INSERT INTO HISTORIAL_LABORAL VALUES ('17','4','2005-11-17','2007-09-4','6','14');
INSERT INTO HISTORIAL_LABORAL VALUES ('10','14','1997-03-6','2003-08-21','6','12');

select top 5 * from empleados;
select top 5 * from trabajos;
select top 5 * from universidades;
select top 5 * from areas_academicas;
select top 5 * from estudios;
select top 5 * from historial_laboral;

