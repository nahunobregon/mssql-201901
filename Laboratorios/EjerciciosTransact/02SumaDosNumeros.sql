/*
Primer ejemplo de TRANSACT SQL
IV Semestre
*/
--T-SQL es No Case Sensitive
DECLARE
@num1 tinyint,
@num2 tinyint, 
@suma tinyint

set @NUM1 = 10 --Asigna el valor de 10
set @num2 = 5

set @suma=@num1+@num2 /*Asinga a @suma la suma de @num1+@num2 */

print 'La suma es: ' + cast(@suma as varchar(10))

