/* EJERCICIOS PROPUESTOS */
1. Script que lea 3 números y los escriba ordenados de forma ascendente.
2. Dados los puntos en el plano cartesiano A(2,5) y B(7,17), se quiere obtener la distancia entre los puntos.
3. De la tabla Personas creada en clase se requiere obtener un codigo autogenerado con el siguiente formato
    PRIMER CARACTER NOMBRE + APELLIDO PATERNO + PRIMER CARACTER APELLIDO MATERNO+AÑO DE NACIMIENTO.
4. Se requiere obtener la cantidad de vocales (a,e,i,o,u) de una palabra.
5. Identificar si una palabra el palindrómica.
6. Realizar un script que permita obtener las raices de una ecuación cuadratica
    Ecuación= ax2+bx+c=0
    Raices: x1,x2=(-b+/-sqrt(b*b-4*a*c))/2*a
7. De la tabla Personas, mostrar aquellos que su salario este entre 2mil y 3mil soles 
Operador BETWEEN
