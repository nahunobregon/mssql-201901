/* Consulta de tablas de la base de datos*/
select top 5 * from autores;
select top 5 * from libros;
select top 5 * from clientes;
select top 5 * from transacciones;

--USO DE LA CLAUSULA INTO
SELECT TOP 100 * 
INTO AUTORES_1 -- NOMBRE DE TABLA A CREAR
FROM AUTORES

--SE CREA LA TABLA SIN COLUMNA IDENTITY
SELECT TOP 100 * 
INTO AUTORES_1 -- NOMBRE DE TABLA A CREAR
FROM AUTORES

SELECT IDENTITY(INT,200,5) ID_NUEVA, NOMBRE, NACIONALIDAD
INTO AUTORES_6
FROM AUTORES_3 --NO TIENE COLUMNA IDENTITY
WHERE NACIONALIDAD ='USA'


-- USO DE LA SENTENCIA LIKE
SELECT * FROM CLIENTES
WHERE NOMBRE_CLI LIKE '%RR%' --% Representa cualquier caracter.

-- LIBROS ESCRITOS ENTRE EL 2000 - 2015
SELECT * FROM LIBROS
WHERE ANIO_LIB BETWEEN 2000 AND 2015


--LIBROS PRESTADOS Y VENDIDOS CON NOMBRE DE CLIENTE
SELECT T.*,C.NOMBRE_CLI FROM TRANSACCIONES AS T
LEFT JOIN CLIENTES AS C ON T.ID_CLI = C.ID_CLI
WHERE T.tipo_tran IN ('VENDIDO','PRESTADO')


--AUTORES QUE NO POSEEN LIBROS ESCRITOS
--132/47
SELECT * FROM AUTORES
WHERE ID_AUT NOT IN (SELECT DISTINCT ID_AUT 
					FROM LIBROS)


--AGRUPAR LIBROS POR AUTOR
SELECT A.*, AUTOR_PRECIO.TOTAL FROM AUTORES A
INNER JOIN 
(SELECT ID_AUT, SUM(PRECIO_LIB) TOTAL 
FROM LIBROS
WHERE PRECIO_LIB IS NOT NULL
GROUP BY ID_AUT) AUTOR_PRECIO
ON A.id_aut = AUTOR_PRECIO.id_aut


--CUANTOS LIBROS POSEE UN AUTOR
SELECT ID_AUT, 
COUNT(ID_LIB) CANT_LIBROS, 
SUM(COPIAS_LIB) TOTAL_COPIAS, 
AVG(PRECIO_LIB) MTO_TOTAL 
FROM LIBROS
GROUP BY ID_AUT;


--PERSONAS POR GENERO
SELECT SEXO_CLI, COUNT(ID_CLI) TOTAL FROM CLIENTES
GROUP BY SEXO_CLI;

--LIBRO MAS CARO Y MAS BARATO
SELECT TOP 1 * FROM LIBROS ORDER BY PRECIO_LIB DESC

SELECT TOP 1 * FROM LIBROS WHERE precio_lib IS NOT NULL
ORDER BY PRECIO_LIB ASC

--
SELECT MIN(PRECIO_LIB) BARATO, MAX(PRECIO_LIB) CARO FROM LIBROS;


--A�OS DE ANTIGUEDAD DEL LIBRO
SELECT L.*, (YEAR(SYSDATETIME()) - ANIO_LIB) ANTIGUEDAD 
FROM LIBROS L;

SELECT * FROM LIBROS 
ORDER BY 4 ASC,5 DESC,COPIAS_LIB DESC;


--TOTAL DE COPIAS DE LIBROS POR A�O
SELECT ANIO_LIB, SUM(COPIAS_LIB) CANT_COPIAS 
FROM LIBROS
GROUP BY ANIO_LIB
HAVING SUM(COPIAS_LIB) < 20
ORDER BY 1 DESC


--INSERTANDO REGISTROS A LA TABLA AUTORES
INSERT INTO AUTORES (NOMBRE_AUT,NACIONALIDAD_AUT)
VALUES ('ALEJANDRO TOLEDO MANRIQUE','PER')

INSERT INTO AUTORES
VALUES ('ERNESTO GARCIA TORRES','PER')

INSERT INTO AUTORES
VALUES 
('ERNESTO GARCIA TORRES','PER'),
('NICOLA PORCHELA','CHI'),
('MATIAS BRIBIO','PER'),
('ALEJANDRA BEINGOREA','USA'),
('MARISOL TELLO','ENG');


SELECT * FROM AUTORES
ORDER BY 1 DESC;

--UPDATE
UPDATE LIBROS 
SET PRECIO_LIB = 40,
CUBIERTA_LIB ='CUERO',
DESC_LIB ='Libros de alta gama',
anio_lib = 2000
WHERE ID_LIB BETWEEN 100 AND 120;


--actualizar 2 a�os a la fecha de modificacion de transaccion
select * from transacciones;

UPDATE TRANSACCIONES
SET FECH_MODIF = DATEADD(YEAR,2,FECH_CREA) 
WHERE TIPO_TRAN = 'VENDIDO'


--ACTUALIZAR @random.com a @senati.edu.pe
UPDATE CLIENTES
SET EMAIL_CLI = 
CONCAT(LEFT(EMAIL_CLI,LEN(EMAIL_CLI)-13),'@SENATI.EDU.PE')
WHERE EMAIL_CLI LIKE '%@random.names'


--DELETE
SELECT TOP 10 * 
INTO DEMO
FROM AUTORES

SELECT * FROM DEMO

INSERT INTO DEMO VALUES 
('GOKU','USA'),
('MARIO','USA'),
('BULMA','USA');

DELETE FROM DEMO
WHERE ID_AUT =12

--REINICIA LAS SECUENCIAS DE LA TABLA
TRUNCATE TABLE DEMO
