create database db_ventas;

use db_ventas;

--Creaci�n de tablas
create table productos(
	id_prod int not null identity(1,1),
	nombre_prod varchar(100),
	codigo_prod char(10),
	estado_prod char(1),
	precio_prod numeric(7,2)
)

create table proveedores (
	id_prov int not null identity(1,1),
	nombre_prov varchar(100),
	dni_prov char(8),
	telef_prov char(10)
)

--Creaci�n de claves primarias (PK)
alter table productos 
add constraint pk_productos primary key (id_prod);

alter table proveedores 
add constraint pk_proveedores primary key (id_prov);

--Creaci�n de claves externas (FK)
alter table productos add id_prov int not null;

alter table productos 
add constraint fk_productos foreign key (id_prov)
references proveedores (id_prov);

--eliminar un constraint
alter table productos drop constraint fk_productos;

--INSERT en tablas
select * from productos;
select * from proveedores;

--Primera inserci�n
insert into proveedores
values 
('Luchito Rosas','45261255','95931245'),
('Pedrito Ramirez','01025645','425-2536'),
('Maria Camarena','78253622','01-452536');

--Segunda inserci�n
insert into productos 
values 
('Teclado','T01','B',25.00,1),
('Mouse','T02','R',10.00,2);


--Eliminando filas de productos
delete productos 
where id_prod = 2;

--Eliminando filas de proveedores
delete proveedores 
where id_prov = 3;