SELECT TOP 5 * FROM EMPLEADOS;
SELECT TOP 5 * FROM TRABAJOS;
SELECT TOP 5 * FROM UNIVERSIDADES;
SELECT TOP 5 * FROM AREAS_ACADEMICAS;
SELECT TOP 5 * FROM ESTUDIOS;
SELECT TOP 5 * FROM HISTORIAL_LABORAL;

/*1. Empleados y supervisores a cargo durante su trayectoria laboral */
select tabla.*, es.nombres_emp nom_supervisor from 
(SELECT HL.*, E.nombres_emp as nom_empleado
FROM HISTORIAL_LABORAL HL 
left JOIN EMPLEADOS E ON HL.id_emp = E.id_emp) tabla
left join empleados es on tabla.supervisor = es.id_emp;

/*2. Empleados, universidad y a�os que estudio */
SELECT ES.*, 
EM.nombres_emp, UN.nombre_univ, 
(cast(ES.anio_egreso_emp as int) - cast(ES.anio_ingreso_emp as int)) anios_estudio
FROM ESTUDIOS ES
LEFT JOIN EMPLEADOS EM ON ES.id_emp = EM.id_emp
LEFT JOIN UNIVERSIDADES UN ON ES.id_univ = UN.id_univ;

/*3. Puestos que ocuparon en su trayectoria laboral*/
SELECT HL.ID_EMP,
E.NOMBRES_EMP, 
HL.ID_TRAB, 
T.NOMBRE_TRAB
FROM HISTORIAL_LABORAL HL 
LEFT JOIN TRABAJOS T ON HL.ID_TRAB = T.ID_TRAB
LEFT JOIN EMPLEADOS E ON HL.ID_EMP = E.ID_EMP
order by HL.id_emp;

/*4. Areas academicas y responsable*/
SELECT HL.*, A.NOMBRE_AREA, E.NOMBRES_EMP
FROM HISTORIAL_LABORAL HL
LEFT JOIN AREAS_ACADEMICAS A ON HL.ID_AREA = A.ID_AREA
LEFT JOIN EMPLEADOS E ON HL.SUPERVISOR = E.ID_EMP;

/*5. De empleados BACHILLER, cuales son de TIC*/
SELECT * FROM ESTUDIOS 
WHERE GRADO_EMP = 'BACHILLER' AND ESPECIALIDAD_EMP ='TECNOLOGIAS DE INFORMACION';


--CREANDO UNA FUNCION QUE CALCULA LAS EDADES DE UNA PERSONA
CREATE FUNCTION obtenerEdad(@id_emp int)
RETURNS INT
AS
BEGIN
	DECLARE @edad int;
	SELECT @edad = datediff(year,fecnac_emp,SYSDATETIME())
	FROM EMPLEADOS
	WHERE ID_EMP = @id_emp;
	return @edad;
END;

select e.*, dbo.obtenerEdad(e.id_emp) as edad
from empleados e;