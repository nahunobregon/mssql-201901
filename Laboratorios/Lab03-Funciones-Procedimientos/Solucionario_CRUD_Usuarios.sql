SELECT TOP 4 * FROM AREAS_ACADEMICAS;
SELECT TOP 4 * FROM EMPLEADOS;
SELECT TOP 4 * FROM ESTUDIOS;
SELECT TOP 4 * FROM HISTORIAL_LABORAL;
SELECT TOP 4 * FROM TRABAJOS;
SELECT TOP 4 * FROM USUARIOS;
SELECT TOP 4 * FROM DEPARTAMENTOS;
SELECT TOP 4 * FROM PROVINCIAS;
SELECT TOP 4 * FROM DISTRITOS;

--SOLUCION
ALTER PROCEDURE sp_reiniciar_ubigeo(@nom_tabla varchar(50))
AS
IF (@nom_tabla is not null) 
	IF (@nom_tabla = 'EMPLEADOS')
		BEGIN
			UPDATE EMPLEADOS
			SET DEPARTAMENTO_EMP = '15',
			provincia_emp='01',
			distrito_emp='01'
			where id_emp is not null;
		END
	ELSE IF (@nom_tabla = 'UNIVERSIDADES')
		BEGIN
			UPDATE UNIVERSIDADES
			SET DEPARTAMENTO_UNIV = '15',
			provincia_univ='01',
			distrito_univ='01'
			where id_univ is not null;
		END
ELSE
	PRINT 'EL NOMBRE DE TABLA NO PUEDE SER NULL';


EXEC sp_reiniciar_ubigeo @nom_tabla='EMPLEADOS';
EXEC sp_reiniciar_ubigeo @nom_tabla='UNIVERSIDADES';

SELECT * FROM EMPLEADOS;
SELECT * FROM UNIVERSIDADES;


--IMPLEMENTANDO CRUD DE LA TABLA USUARIOS
--01 INSERT
CREATE procedure sp_insert_usuarios(@login varchar(50), @clave varchar(50))
as
/*
	DECLARE @CARACTER CHAR(1);
	SELECT @CARACTER = UPPER(SUBSTRING(NOMBRES_EMP,1,1))
	FROM EMPLEADOS
	WHERE ID_EMP=1;
*/
	IF (LEN(@clave)>5) -- AND @CARACTER=UPPER(SUBSTRING(@login,1,1)))
		BEGIN
			INSERT INTO USUARIOS (login_user,clave_user)
			VALUES (@login,@clave);
			SELECT * FROM USUARIOS ORDER BY 1 DESC;
		END;
	ELSE
		PRINT 'LOGIN INCORRECTA';
		
--EJECUTANDO INSERT A USUARIOS
EXEC sp_insert_usuarios @login='GABRIEL', @clave='1234567';


--02 DELETE
CREATE PROCEDURE sp_delete_usuario (@login varchar(50))
as
	DELETE USUARIOS WHERE login_user = @login;
	SELECT * FROM USUARIOS ORDER BY 1 DESC;
	
	--EJECUTANDO
	EXEC sp_delete_usuario @login='dba'


--UPDATE
ALTER PROCEDURE sp_update_usuario(@login varchar(50), 
								@clave varchar(50),
								@estado char(1),
								@id_user int
								)
AS
UPDATE USUARIOS 
	SET LOGIN_USER = @login,
		CLAVE_USER = @clave,
		ESTADO_USER = @estado
	WHERE id_user = @id_user;

	SELECT * FROM USUARIOS ORDER BY 1 DESC;

	--Ejecutando
	EXEC sp_update_usuario @login='INVITADO',
							@clave='INVITADO',
							@estado='A',
							@id_user=6

--SELECT
CREATE PROCEDURE sp_listar_usuarios
as
	SELECT * FROM USUARIOS
	ORDER BY 1 DESC;

	--Ejecutando
	EXEC sp_listar_usuarios;


