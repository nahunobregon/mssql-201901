--Ejercicios de Funciones
--obtener la edad de un empleado

IF OBJECT_ID('dbo.obtener_edad','FN') IS NOT NULL
	DROP FUNCTION dbo.obtener_edad;

CREATE FUNCTION fn_obtener_edad(@id_emp INT)
RETURNS INT
AS
	BEGIN
		DECLARE @edad INT;
		
		SELECT 
		@edad = DATEDIFF(YEAR,fecnac_emp,SYSDATETIME())
		FROM EMPLEADOS
		WHERE ID_EMP = @id_emp;

		RETURN @edad;
		 
	END;


DECLARE @mi_edad INT
BEGIN
SET @mi_edad = dbo.obtener_edad(20)
PRINT concat(@mi_edad,' a�os')
END;


select id_emp, concat(nombres_emp,' ',apepat_emp) nombres, 
fecnac_emp, concat(dbo.obtener_edad(id_emp),' a�os') edad 
from empleados;


--Funcion que obtiene segmentos de empleados por edad
CREATE FUNCTION fn_listar_emp_edad(
							@sexo_emp char(1), 
							@edad int)
RETURNS TABLE
AS
RETURN
(
	SELECT ID_EMP, DNI_EMP, 
	CONCAT(NOMBRES_EMP,' ',APEPAT_EMP,' ',APEMAT_EMP) NOMBRES,
	dbo.fn_obtener_edad(ID_EMP) edad
	FROM EMPLEADOS
	WHERE SEXO_EMP = @sexo_emp
	AND DATEDIFF(YEAR,fecnac_emp,SYSDATETIME()) > @edad
);

--ejecutar
select * from fn_listar_emp_edad('F',18);


--FUNCION QUE MUESTRA EL SALDO PRESUPUESTAL DE LAS AREAS ACADEMICAS
CREATE FUNCTION fn_listar_saldo_areas(@monto numeric(7,2))
returns @table TABLE(
	ID_AREA INT NOT NULL IDENTITY(1,1),
	DESC_AREA VARCHAR(100) NOT NULL,
	PRESUPUESTO_AREA NUMERIC(7,2) NOT NULL,
	SALDO NUMERIC(7,2) NOT NULL
)
AS
	BEGIN
		INSERT @table
		SELECT 
		concat(trim(codigo_area),' - ',nombre_area) DESC_AREA,
		presupuesto_area,
		(presupuesto_area - presupuesto_actual_area) SALDO
		FROM AREAS_ACADEMICAS
		WHERE presupuesto_area >= @monto
		RETURN;
	END;

--Ejecutar
SELECT * FROM fn_listar_saldo_areas(15000);
