--SOLUCIONARIO LAB03
--02
SELECT * FROM EMPLEADOS;

ALTER TABLE EMPLEADOS ADD id_area int null;

--03
alter table empleados alter column departamento_emp char(2) null;
alter table empleados alter column provincia_emp char(2) null;
alter table empleados alter column distrito_emp char(2) null;

update EMPLEADOS
set departamento_emp = null,
provincia_emp = null,
distrito_emp = null
where id_emp is not null;

update UNIVERSIDADES
set departamento_univ = null,
provincia_univ = null,
distrito_univ = null
where id_univ is not null;

alter table UNIVERSIDADES alter column departamento_univ char(2) null;
alter table UNIVERSIDADES alter column provincia_univ char(2) null;
alter table UNIVERSIDADES alter column distrito_univ char(2) null;

select * from UNIVERSIDADES;

--04
CREATE TABLE USUARIOS(
	id_user int not null identity(1,1),
	login_user varchar(20),
	clave_user varchar(20),
	estado_user char(1),
	fecha_registro_user date
)

alter table usuarios add constraint pk_usuarios primary key (id_user);

--funciones
--02 fn_calcula_anios_servicio (@id_emp)
ALTER FUNCTION fn_calcula_anios_servicio(@id_emp int)
returns int
as
begin
	declare @anio_servicio int; 
	select 
	@anio_servicio = datediff(year,fecha_inicio,fecha_fin) 
	from historial_laboral hl 
	where id_emp = @id_emp

	IF (@anio_servicio IS NULL)
		SET @anio_servicio = 0
	return @anio_servicio
end;

select id_emp, nombres_emp,  
dbo.fn_calcula_anios_servicio(id_emp) anios_servicio
from empleados;


--03 fn_valida_anios_servicio(@id_emp)
alter function fn_valida_anios_servicio(@id_emp int)
returns bit
as
begin 
	declare @total_anios int; 
	declare @rpta bit = 1; 
	select 
	@total_anios = dbo.fn_calcula_anios_servicio(id_emp)
	from empleados
	where id_emp = @id_emp

	if(@total_anios < 3)
		set @rpta = 0
	return @rpta
end;

select id_emp, nombres_emp,  
dbo.fn_valida_anios_servicio(id_emp) validado
from empleados;