--FUNCION que valida credenciales de usuarios
alter FUNCTION fn_valida_credenciales(@login varchar(50), @clave varchar(50))
RETURNS VARCHAR(200)
AS
	BEGIN
		--CUERPO DE LA FUNCION
		--1. Clave mas de 5 caracteres(*)
		--2. Login debe ser mayor a 3 caracteres
		----3. La clave debe tener minimo un n�mero
		--4. la clave y login deben ser diferentes
		DECLARE @rpta VARCHAR(200);
		DECLARE @mensaje varchar(200);
		IF(LEN(@clave)<=5)
			BEGIN
				set @mensaje = 'CLAVE INCORRECTA'
				set @rpta = '0';
			END
		ELSE IF  (LEN(@login)<=3)
			BEGIN
				set @mensaje = 'USUARIO INCORRECT0'
				set @rpta = '0';
			END
		ELSE IF (@login = @clave)
			BEGIN
				set @mensaje = 'USUARIO Y CLAVE TIENEN QUE SER DIFERENTES'
				set @rpta = '0';
			END
		ELSE
			BEGIN
				set @mensaje = 'BENVENIDO AL SISTEMA'
				set @rpta = '1';
			END
	
		IF (@rpta = '0')
			set @rpta = @mensaje;
		ELSE
			set @rpta = @rpta + '-'+@mensaje;

		RETURN @rpta;
	END;

	--Ejecutar
	declare @respuesta varchar(200);
	begin
		set @respuesta =  dbo.fn_valida_credenciales ('CARLOSVELIZ1','carlosveliz');
		print @respuesta;
	end;