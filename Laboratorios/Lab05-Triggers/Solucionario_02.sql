--IMPLEMENTAR AUDITORIA A LA TABLA EMPLEADOS
SELECT * FROM EMPLEADOS;

--01CREAR LAS COLUMNAS DE AUDITORIA
ALTER TABLE EMPLEADOS ADD USER_CREA VARCHAR(50) NULL;
ALTER TABLE EMPLEADOS ADD FECH_CREA DATETIME NULL;
ALTER TABLE EMPLEADOS ADD USER_MODI VARCHAR(50) NULL;
ALTER TABLE EMPLEADOS ADD FECH_MODI DATETIME NULL;

--02Crear un SP de inserción de filas
ALTER PROCEDURE SP_INSERT_EMPLEADO (
								@DNI char(8),
								@NOMBRES varchar(100),
								@APEPAT varchar(100),
								@APEMAT varchar(100),
								@DIRECCION1@ varchar(200), 
								@SEXO char(1))
AS
	DECLARE @CANT INT
	SELECT @CANT = COUNT(*) FROM EMPLEADOS WHERE DNI_EMP=@DNI;
	IF(LEN(@DNI)=8 AND @CANT=0)
		BEGIN
			INSERT INTO EMPLEADOS 
			(DNI_EMP,NOMBRES_EMP,APEPAT_EMP,APEMAT_EMP,DIRECCION1_EMP, SEXO_EMP)
			VALUES
			(@DNI,@NOMBRES,@APEPAT,@APEMAT,@DIRECCION1@,@SEXO)
		END
	ELSE
		PRINT 'DNI ERRONEO Y/O EL DNI YA EXISTE'
	SELECT * FROM EMPLEADOS ORDER BY ID_EMP DESC;

--02Crear un SP de modificación de filas
ALTER PROCEDURE SP_UPDATE_EMPLEADO(@DNI char(8),@NOMBRES varchar(100),
								@APEPAT varchar(100),@APEMAT varchar(100),
								@DIRECCION1@ varchar(200), 
								@SEXO char(1),@FECNAC DATE,
								@SALARIO NUMERIC(7,2),@BONIFICACION NUMERIC(7,2),
								@ID INT)
AS
	UPDATE EMPLEADOS 
	SET DNI_EMP		=ISNULL(@DNI,DNI_EMP),
	NOMBRES_EMP		=ISNULL(@NOMBRES,NOMBRES_EMP),
	APEPAT_EMP		=ISNULL(@APEPAT,APEPAT_EMP),
	APEMAT_EMP		=ISNULL(@APEMAT,APEMAT_EMP),
	DIRECCION1_EMP	=ISNULL(@DIRECCION1@,DIRECCION1_EMP), 
	SEXO_EMP		=ISNULL(@SEXO,SEXO_EMP),
	FECNAC_EMP		=ISNULL(@FECNAC,FECNAC_EMP),
	SALARIO_EMP		=ISNULL(@SALARIO,SALARIO_EMP),
	BONIFICACION_EMP=ISNULL(@BONIFICACION,BONIFICACION_EMP)
	WHERE ID_EMP=@ID

	SELECT * FROM EMPLEADOS ORDER BY ID_EMP DESC;

--03Ejecutando SP
EXEC dbo.SP_INSERT_EMPLEADO '40404040','JULIO','MONTES','ARCE','LIMA - CALLAO','M';
EXEC dbo.SP_UPDATE_EMPLEADO NULL,NULL,NULL,NULL,NULL,NULL,'12/05/2012',5000,150,22;

SELECT ISNULL(NULL,'SOY NULO');

--04CREANDO TRIGGER
CREATE TRIGGER TIU_EMPLEADOS ON EMPLEADOS
AFTER INSERT,UPDATE
AS
BEGIN
	IF EXISTS (SELECT * FROM inserted) AND EXISTS (SELECT * FROM deleted)
		BEGIN     
			--UPDATE
			PRINT 'EL USUARIO INTENTA ACTUALIZAR'
			DECLARE @ID INT
			SELECT @ID = ID_EMP FROM Inserted

			UPDATE EMPLEADOS 
			SET USER_MODI = SYSTEM_USER, FECH_MODI = GETDATE()
			WHERE ID_EMP=@ID
		END

	IF EXISTS (SELECT * FROM Inserted) AND NOT EXISTS (SELECT * FROM deleted)
		BEGIN
			--INSERT
			PRINT 'EL USUARIO INTENTA INSERTAR'
			DECLARE @ID_EMP INT
			SELECT @ID_EMP = ID_EMP FROM Inserted

			UPDATE EMPLEADOS 
			SET USER_CREA = SYSTEM_USER, FECH_CREA = GETDATE(),
				USER_MODI = SYSTEM_USER, FECH_MODI = GETDATE()
			WHERE ID_EMP=@ID_EMP
		END
END

--01 AUDITORIA DE LA TABLA HISTORIAL LABORAL
SELECT * FROM HISTORIAL_LABORAL;